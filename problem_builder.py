import numpy as np
import pandas as pd

n = 2 # Кол-во узлов
k = 4 # Кол-во таблиц
N = np.array([10000, 15000, 3000, 2000]) # Кол-ва строк
e = np.array([109, 20, 64, 71]) # Максимальный размер одной строки данных для каждой таблицы
L = N * e # Объем каждой таблицы
B = L.sum() # Объем всех таблиц
b = [1000000, 1400000] # Объем памяти каждого узла

m = 5 # Кол-во запросов
#размер одной строки данных (в байтах) таблицы, пересылаемой при выполнении запроса
d = np.array([
    [0, 0, 24, 0],
    [44, 0, 0, 0],
    [44, 4, 44, 0],
    [0, 0, 40, 44],
    [44, 4, 48, 4]
])
# Интесивность
lamda = np.array([
    [0.75, 0, 0, 0.25, 0],
    [0.1, 0.3, 0.25, 0.1, 0.25]
])

C = 0
for j in range(0, n): # Узлы
    for l in range(0, k): # Таблицы
        for i in range(0, m): # Запросы
            C += lamda[j][i] * d[i][l] * N[l]

x = []
for l in range(0, k): # Таблицы
    for j in range(0, n): # Узлы
        constl = 0
        for i in range(0, m): # Запросы
            constl += lamda[j][i] * d[i][l] * N[l]
        x.append(constl)

        
# Постановка задачи
goal = []
restrictions = []
for index, xi in enumerate(x):
    if (index % 2 == 1):
        restrictions.append("x" + str(index) + "+" + "x" + str(index+1) + " = 1")
    goal.append(str(int(xi)) + "*x" + str(index+1))
goal = "+".join(goal)

for j in range(0, n): # Узлы
    const_l = []
    for l in range(0, k): # Таблицы
        x_str = "x"
        if (j % 2 == 0):
            x_str += str((j+l)*2 + 1)
        else:
            x_str += str((j+l)*2)
        const_l.append(x_str + "*" + str(L[l]))
    restrictions.append("+".join(const_l) + "<=" + str(b[j]))
        
        
print(str(int(C)) + "- (" + goal + ") -> min")
print("\n".join(restrictions))